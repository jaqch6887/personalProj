import { Component, OnInit, Input, OnChanges, DoCheck } from '@angular/core';
import { ItemInputBase } from '../model/item-input-base';
@Component({
  selector: 'app-custom-form',
  templateUrl: './custom-form.component.html',
  styleUrls: ['./custom-form.component.css'],
})
export class CustomFormComponent implements OnInit, OnChanges, DoCheck {
 myModel: ItemInputBase = new ItemInputBase('lbl textapp','normal', 'left','linear', 'text', 'value', 'alternative text filled ',true, 30, 30, 70);
  @Input() modelForm = {
    title: 'this is my title',
    event: ()=> {
      console.log('hola que tal este es el evento de prueba');   
    },
    elementsForm:[this.myModel]
  }
  titleForm = '';
  constructor() { }

  ngOnInit() {
    this.titleForm = this.modelForm.title;
  }

  ngOnChanges() {
  }

  ngDoCheck() {
    this.titleForm = this.modelForm.title;
  }

}
