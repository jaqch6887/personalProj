import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { CustomFormModule } from './custom-form/custom-form.module';
import { FormBuilderComponent } from './form-builder/form-builder.component';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent,
    FormBuilderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    CustomFormModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
