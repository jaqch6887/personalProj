import { ItemInputBase } from './item-input-base';

export class ItemCheckModel extends ItemInputBase {
  checked: boolean;
  name: string;
  constructor(
    label = '', itemAlign = 'normal', 
    labelAlignText = 'left', placeholder='', 
    type = '', value:any = '', alternativeValue = '', isReadOnly = false, 
    height:any = 0, widthLabel:any = 0, widthInput:any = 0, checked = false, name = ''
  ) {
    super(label, itemAlign, 
      labelAlignText, placeholder, 
      type, value, alternativeValue, isReadOnly, 
      height, widthLabel, widthInput);
    this.checked = checked;
    this.name = name;
  }

  getName() {
    return this.name;
  }

  getCheckedValue() {
    return this.checked;
  }

}