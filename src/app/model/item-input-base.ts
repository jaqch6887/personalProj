export class ItemInputBase {
  label: string;
  itemAlign: string;//align input item 'normal' 'inverse' 'stacked'
  labelAlignText: string;
  placeholder: string;  
  type: string;//type item 'text' 'password' 'checkbox' 'radio' 
  value: string;
  alternativeValue: string;
  isReadOnly: boolean;
  height: number;
  widthLabel: number;
  widthInput: number;
  constructor(label = '', itemAlign = 'normal', 
  labelAlignText = 'left', placeholder='', 
  type = '', value = '', alternativeValue = '', isReadOnly = false, 
  height:any = 0, widthLabel:any = 0, widthInput:any = 0 ) {
    this.label = label;
    this.itemAlign = itemAlign;
    this.labelAlignText = labelAlignText;
    this.placeholder = placeholder
    this.type = type;
    this.value = value;
    this.alternativeValue = alternativeValue;
    this.isReadOnly = isReadOnly;
    this.height = height;
    this.widthLabel = widthLabel;
    this.widthInput = widthInput;
  }

}