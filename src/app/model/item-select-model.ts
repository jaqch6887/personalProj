import { ItemInputBase } from './item-input-base';

export class ItemSelectModel extends ItemInputBase {
  options: string[];
  isApplyDefault = true;
  constructor(
    label = '', itemAlign = 'normal', 
    labelAlignText = 'left', placeholder='', 
    type = '', value = '', alternativeValue = '', isReadOnly = false, 
    height = 0, widthLabel = 0, widthInput = 0, options = []
  ) {
    super(label, itemAlign, 
      labelAlignText, placeholder, 
      type, value, alternativeValue, isReadOnly, 
      height, widthLabel, widthInput);
    this.options = options;
  }

  getSelected() {
    if(this.isApplyDefault) {
      return this.options.length===0?'': this.options[0];
    }
  }
}