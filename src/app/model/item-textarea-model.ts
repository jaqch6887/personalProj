import { ItemInputBase } from './item-input-base';

export class ItemTextareaModel extends ItemInputBase {
  checked: boolean;
  name: string;
  constructor(
    label = '', itemAlign = 'normal', 
    labelAlignText = 'left', placeholder='', 
    type = '', value = '', alternativeValue = '', isReadOnly = false, 
    height = 0, widthLabel = 0, widthInput = 0
  ) {
    super(label, itemAlign, 
      labelAlignText, placeholder, 
      type, value, alternativeValue, isReadOnly, 
      height, widthLabel, widthInput);
    
  }

  getName() {
    return this.name;
  }
}