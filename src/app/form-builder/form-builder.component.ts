import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { ItemInputBase } from '../model/item-input-base';
import { ItemCheckModel } from '../model/item-check-model';
import { ItemSelectModel } from '../model/item-select-model';

var titleForm = 'title test';
@Component({
  selector: 'app-form-builder',
  templateUrl: './form-builder.component.html',
  styleUrls: ['./form-builder.component.css'],
})
export class FormBuilderComponent implements OnInit, OnChanges {
  inputTitleForm:  ItemInputBase = new  ItemInputBase('title of Form','normal', 'right','enter your title', 'text', '', '', false, 18, 30, 70);
  labelValue:  ItemInputBase = new  ItemInputBase('text in label','normal', 'right','enter your text', 'text', '', '', false, 18, 30, 70);
  alignOptions: ItemInputBase = new ItemSelectModel('Select Component Alignment ','normal', 'right','', 'select', '', '', false, 18, 30, 70, ['normal', 'inverse', 'stacked']);
  alignLabelText: ItemInputBase = new ItemSelectModel('Alignment Label ','normal', 'right','', 'select', '', '', false, 18, 30, 70, ['left', 'right']);
  inputPlaceHolder: ItemInputBase = new  ItemInputBase('Placeholder','normal', 'right','enter your placeholder', 'text', '', '', false, 18, 30, 70);
  selectType: ItemSelectModel = new ItemSelectModel('Select type element ','normal', 'right','', 'select', '', 'select option', false, 18, 30, 70, ['text', 'textarea', 'password','checkbox', 'select']);
  inputValue:  ItemInputBase = new  ItemInputBase('value Item','normal', 'right','enter your value', 'text', '', '', false, 18, 30, 70);

  readOnlyValue: ItemCheckModel = new ItemCheckModel('is ReadOnly?','normal', 'right', '', 'checkbox', 'false', '', false, 18, 30, 70, false, '');
  heightValue:  ItemInputBase = new  ItemInputBase('Height','normal', 'right','enter your height', 'text', '', '', false, 18, 30, 70);
  widthLabelValue:  ItemInputBase = new ItemInputBase('Width of Label','normal', 'right','enter your width only number', 'text', '', '', false, 18, 30, 70);
  widthInputValue:  ItemInputBase = new ItemInputBase('Width of Input Item','normal', 'right','enter your width only number', 'text', '', '', false, 18, 30, 70);
  checkedValue: ItemCheckModel = new ItemCheckModel('is Checked?','normal', 'right', '', 'checkbox', 'false', '', false, 18, 30, 70, false, '');
  nameValue:  ItemInputBase = new ItemInputBase('Name of Input','normal', 'right','enter your name', 'text', '', '', false, 18, 30, 70);

  listNewElementsForm = [];
  titleOfNewForm = 'testing form';

  modelCreator = {
    title: 'Enter your properties' ,
    event: ()=> { 
      let indice = 0;      
      let elements:any[] = this.modelCreator.elementsForm;
      
      let newValue: any; 

      if(elements[5].value === 'checkbox') {
        //console.log('val 7', elements[7].getCheckedValue());
        newValue = new ItemCheckModel(
        elements[1].value, elements[2].value, elements[3].value,elements[4].value,elements[5].value, elements[6].value, '', elements[7].getCheckedValue(), elements[8].value, elements[9].value, elements[10].value,  elements[11].getCheckedValue(),elements[12].value);
      } else {
        if(elements[5].value === 'select') {
          //console.log('val 7', elements[7].getCheckedValue());
          let itemsSelec = elements[6].value.split(',');
          newValue =
            new ItemSelectModel(elements[1].value, elements[2].value, elements[3].value,elements[4].value,elements[5].value, elements[6].value, '',elements[7].getCheckedValue(), elements[8].value, elements[9].value, elements[10].value, itemsSelec);         
          newValue.isApplyDefault = false;
        } else {
          //console.log('para el readonly', elements[7].getCheckedValue());
          newValue = new ItemInputBase(
          elements[1].value, elements[2].value, elements[3].value,elements[4].value,elements[5].value, elements[6].value, '', elements[7].getCheckedValue(), elements[8].value, elements[9].value, elements[10].value);
        }
      }
        
      if(elements[0].value) {
        this.myNewForm.title = elements[0].value;
        this.listNewElementsForm.push(newValue);     
      } else {
        alert('Insert title of form');
      }  
    },
    elementsForm: [
      this.inputTitleForm,
      this.labelValue,
      this.alignOptions, 
      this.alignLabelText,
      this.inputPlaceHolder,
      this.selectType, 
      this.inputValue, 
      this.readOnlyValue,
      this.heightValue,
      this.widthLabelValue,
      this.widthInputValue,
      this.checkedValue,
      this.nameValue
    ]
  }
  
  myNewForm = { 
    title: titleForm,
    event: ()=> { 
      let indice = 0;
      let allValues = '';
      console.log('************************* SHOW ALL VALUES FROM MY NEW FORM *************************')
      this.myNewForm.elementsForm.forEach((value) => {
        if(value.type === 'checkbox' || value.type === 'radio') {
          console.log('valueChecked : ',value.getCheckedValue());
          allValues = allValues + value.getCheckedValue()+ ', ';
        } else {
          console.log('value[', indice++, '].- ',value.value);
          allValues = allValues + value.value + ', ';
        }

      })
      console.log('************************* END *************************')
      alert(allValues);
    },
    elementsForm: this.listNewElementsForm
  }

  constructor() { }

  ngOnInit() {    
  }

  ngOnChanges() {
    this.myNewForm.title = titleForm;
  }

}
