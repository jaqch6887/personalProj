import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//import { CustomFormComponent } from '../custom-form/custom-form.component';
import { InputComponent } from '../input/input.component';
import { FormsModule } from '@angular/forms';
import { AlignerDirective } from '../input/aligner-directive';
import { MyDirective } from '../input/my-directive';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
  ],
  declarations: [ InputComponent, AlignerDirective,MyDirective],
  exports: [ InputComponent ]
})
export class CustomFormModule { }
