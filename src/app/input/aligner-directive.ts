import { Directive, ElementRef, Input, HostListener, OnInit, OnChanges } from '@angular/core';
@Directive({ selector: '[appAlignerDirective]' })
export class AlignerDirective implements  OnInit, OnChanges {  
  @Input() width: number = 100;
  @Input() height: number = 100;
  @Input() isByPercentage: boolean = false;
  @Input() alingnText: string = 'left';
  @Input() alignElement: string = '';
  @Input() isInBlock: boolean = true;
  @Input() fontSize: number = 18;
  @Input() nameDirective: string = '';
  @Input() typeElement: string = '';
  unitSize = {
    percent: '%',
    pixel: 'px'
  };

  constructor(private elementRef: ElementRef) {
    
  }

  ngOnInit() {
    //this.elementRef.nativeElement['name'] = this.nameDirective;
    this.setName();
    this.setSize();
    this.setFontSize();
    this.setAlignText();
    //this.setAlignText();
    //this.setAlignElement();
    //this.setBlockBehaviour();
  }

  ngOnChanges() {
    this.setSize();
    //this.setAlignText();
    //this.setAlignElement();
  }

  setSize() {
    let unity = this.unitSize.pixel;

    if(this.isByPercentage) {
      unity = this.unitSize.percent;
    }

    this.setWidth(unity);
    this.setHeight();
  }

  setWidth(unit) {
    /*if(!this.isInBlock) {
      this.elementRef.nativeElement.style.width = '100%';  
    } else {*/
      this.elementRef.nativeElement.style.width = `${this.width}${unit}`;
    //}
  }

  setHeight() {
    //console.log('this is the height res: ', this.isInBlock, '--> ', this.height);
    /*if(!this.isInBlock) {
      this.elementRef.nativeElement.style.height = `100%`;
    } else {*/
      this.elementRef.nativeElement.style.height = `${this.height}px`;
    //}
  }

  setFontSize() {    
    this.elementRef.nativeElement.style.fontSize = `${this.fontSize}px`;
    if(this.typeElement !== 'textarea') {
      this.elementRef.nativeElement.style.lineHeight = `${this.height}px`;
    }
  }

  setAlignText() {
    this.elementRef.nativeElement.style.textAlign = `${this.alingnText}`;
  }

  setName() {
    this.elementRef.nativeElement.name = this.nameDirective;
  }

}
