import { Component, OnInit,OnChanges, Input, DoCheck } from '@angular/core';
import { ItemInputBase } from '../model/item-input-base';

const rightPosition: string = 'right';
const leftPosition: string = 'left';
@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css'],
})

export class InputComponent implements OnInit, OnChanges {
  @Input() model:any;
  isAlignInverse: boolean = false;
  widthInputElement: number;
  widthLabelElement: number;
  isBlockView: boolean = false;
  fontSizeElements: number = 30;
  fontSizeItem:number;
  heightItem: number;
  isReadOnly = false;
  checkedValue = false;
  inputName: string;
  constructor() { }

  ngOnInit() {
    this.fontSizeElements = this.model.height;
    this.widthInputElement = this.model.widthInput;    
    this.widthLabelElement = this.model.widthLabel;
    this.isReadOnly = this.model.isReadOnly;    
    this.inputName = this.model.name;
    this.setSelectedDefaultValue();
    this. setDisplayMode();
    this.setFontSize();
    this.setCheckedValue();
  }


  ngOnChanges() {
    this.setCheckedValue();
  }

  getName() {
    return this.model.getName? this.model.getName(): '';
  }
  setFontSize() {
    this.fontSizeItem = this.fontSizeElements>14? this.fontSizeElements - 8: this.fontSizeElements;
  }

  setDisplayMode() {
    if (this.model.itemAlign === 'normal') {
      this.isAlignInverse = false;
    }

    if (this.model.itemAlign === 'inverse') {
      this.isAlignInverse = true;
    }

    if (this.model.itemAlign === 'stacked') {
      this.isBlockView = true;
      this.widthLabelElement = 100;
      this.widthInputElement = 100;
      this.heightItem = this.fontSizeElements*2;
    } else {
      this.isBlockView = false;
      this.heightItem = this.fontSizeElements;
    }

  }

  setCheckedValue() {
    if(this.model.type === 'checkbox') {
      this.checkedValue = this.model.checked;
    } else {
      this.checkedValue = false;
    }
  }

  setSelectedDefaultValue() {
    if(this.model.type === 'select' && this.model.isApplyDefault) {
      this.model.value = this.model.type === 'select'? this.model.getSelected(): this.model.value;
    }
  }

  changeCheck() {
    if(this.model.type ==='checkbox') {
      this.checkedValue = !this.checkedValue;      
      this.model.checked= this.checkedValue;
    }
  }

}
