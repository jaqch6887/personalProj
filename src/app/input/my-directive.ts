import { Directive, ElementRef, Input, HostListener, OnInit, OnChanges } from '@angular/core';

@Directive({ selector: '[appMyD]' })
export class MyDirective implements  OnInit, OnChanges {  
  @Input() roundNumber: number = 0;

  constructor(private elementRef: ElementRef) {
  }

  ngOnInit() {
    this.setRound();
  }

  ngOnChanges() {
    this.setRound();
  }

  setRound() {
    this.elementRef.nativeElement.style.borderRadius = `${this.roundNumber}px`;
  }


}
