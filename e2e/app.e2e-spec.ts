import { EmailproyPage } from './app.po';

describe('emailproy App', function() {
  let page: EmailproyPage;

  beforeEach(() => {
    page = new EmailproyPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
